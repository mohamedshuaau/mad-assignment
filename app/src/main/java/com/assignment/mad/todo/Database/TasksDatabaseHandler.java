package com.assignment.mad.todo.Database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.widget.Toast;

import com.assignment.mad.todo.Models.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * This class houses all the methods required for CRUD on tasks table
 * There can be multiple of these classes if the application had more than one "module"
 * But as the application has only one module (tasks) which handles only one model, single class for that
 * module is created
 */
public class TasksDatabaseHandler {

    // table name
    protected String TABLE_NAME = "tasks";

    // context
    protected Context context;

    // string columns constants
    protected String
            COLUMN_ID = "id",
            COLUMN_NAME = "name",
            COLUMN_SHORT_DESCRIPTION = "short_description",
            COLUMN_DESCRIPTION = "description",
            COLUMN_STATUS = "status",
            COLUMN_CREATED_AT = "created_at",
            COLUMN_UPDATED_AT = "updated_at",
            COLUMN_DELETED_AT = "deleted_at";

    // tasks list
    protected List<Task> tasks;

    // create a variable for database helper to be instantiated on constructor
    protected SQLiteDatabaseHelper databaseHelper;

    // task states pending and complete
    protected String TASK_STATE_COMPLETED = "completed";

    // default constructor
    public TasksDatabaseHandler(Context context) {
        // instantiate sqlite database helper class
        databaseHelper = new SQLiteDatabaseHelper(context);
        // assign context
        this.context = context;
    }

    /**
     * Creates a new Task
     *
     * @param task task object
     * @return long id of the task
     */
    public long createNewTask(Task task) {

        // no try catches no nothing. just a simple query
        // write mode
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        // create content values and add the values to each of the column
        // columns created and updated will be defaulted to current time stamp
        // deleted at and status will be set to null by default
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, task.getTaskName());
        values.put(COLUMN_SHORT_DESCRIPTION, task.getShortDescription());
        values.put(COLUMN_DESCRIPTION, task.getDescription());

        // insert the element and return the id of the newly added row
        return db.insert(TABLE_NAME, null, values);

    }

    /**
     * Get all the tasks from the Database
     *
     * @return Task tasks
     */
    public List<Task> getTasks() {

        // read mode
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        // from docs
        // sort a column. in this case, id by desc
        String sortOrder = COLUMN_ID + " DESC";

        // filter results
        // where deleted at is null
        String selection = COLUMN_DELETED_AT + " IS NULL";

        // store the data after getting it to a cursor
        @SuppressLint("Recycle") Cursor cursor = db.query(TABLE_NAME, null, selection, null, null, null, sortOrder);

        tasks = new ArrayList<Task>();
        tasks.clear();
        while(cursor.moveToNext()) {

            // get all columns
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
            String shortDescription = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SHORT_DESCRIPTION));
            String description = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DESCRIPTION));
            String status = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_STATUS));
            String createdAt = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CREATED_AT));
            String updatedAt = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_UPDATED_AT));
            String deletedAt = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DELETED_AT));

            // create a new task from the rows got from database
            Task task = new Task(id, name, shortDescription, description, status, createdAt, updatedAt, deletedAt);

            // add to the list
            tasks.add(task);
        }

        // close cursor
        cursor.close();

        // return tasks
        return tasks;
    }

    /**
     * Update the task status
     *
     * @param task task
     * @return int count
     */
    public int updateTaskStatus(Task task) {

        // writeable
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String state = TASK_STATE_COMPLETED;

        // if the current tasks current status is not empty, then assume its currently in completed state
        // set it to pending again if so
        if (! TextUtils.isEmpty(task.getStatus())) {
            state = "";
        }

        ContentValues values = new ContentValues();
        values.put(COLUMN_STATUS, state);

        // updating the column id with the current tasks id
        String selection = COLUMN_ID + " LIKE ?";
        String[] selectionArgs = { Integer.toString(task.getId()) };

        // return db count
        return db.update(
                TABLE_NAME,
                values,
                selection,
                selectionArgs);

    }

    /**
     * Delete the task
     * This is not a delete call per say
     * It simply stamps something on to deleted column which then the task get function will ignore
     * Essentially a soft delete
     * Technically this is still a delete as I want to keep the records in the db but not show them on select query
     *
     * @param task task
     * @return int count
     */
    public int deleteTask(Task task) {

        // writeable
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_DELETED_AT, "CURRENT DATE STAMP");

        // updating the column id with the current tasks id
        String selection = COLUMN_ID + " LIKE ?";
        String[] selectionArgs = { Integer.toString(task.getId()) };

        // return db count
        return db.update(
                TABLE_NAME,
                values,
                selection,
                selectionArgs);

    }

    /**
     * Edit/Update the task
     *
     * @param task task
     * @return int count
     */
    public int updateTask(Task task) {

        // writeable
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, task.getTaskName());
        values.put(COLUMN_SHORT_DESCRIPTION, task.getShortDescription());
        values.put(COLUMN_DESCRIPTION, task.getDescription());

        // updating the column id with the current tasks id
        String selection = COLUMN_ID + " LIKE ?";
        String[] selectionArgs = { Integer.toString(task.getId()) };

        // return db count
        return db.update(
                TABLE_NAME,
                values,
                selection,
                selectionArgs);

    }

}
