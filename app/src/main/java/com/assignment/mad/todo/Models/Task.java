package com.assignment.mad.todo.Models;

/**
 * Task Model
 * This is the task model responsible for handling/structuring task related data
 * This model is used to create, update, delete, read task data from database
 */
public class Task {

    // integer task id
    protected int id;
    // string task name, short description, description, created at, updated at, deleted at
    // by default, status, created at, updated at and deleted at will be null
    protected String taskName, shortDescription, description, status, created_at = "", updated_at = "", deleted_at = "";

    /**
     * Default Task Constructor
     * This constructor ignores id, status, created at, updated at and deleted at fields
     *
     * @param taskName task name
     * @param shortDescription task short description
     * @param description task long description
     */
    public Task(String taskName, String shortDescription, String description) {
        this.taskName = taskName;
        this.shortDescription = shortDescription;
        this.description = description;
    }

    /**
     * Overload Constructor
     * This constructor ignores created at, updated at and deleted at fields
     *
     * @param id task id
     * @param taskName task name
     * @param shortDescription task short description
     * @param description task long description
     * @param status task status
     */
    public Task(int id, String taskName, String shortDescription, String description, String status) {
        this.id = id;
        this.taskName = taskName;
        this.shortDescription = shortDescription;
        this.description = description;
        this.status = status;
    }

    /**
     * Overload Constructor
     * This Constructor will accept created at, updated at and deleted at dates
     *
     * @param id task id
     * @param taskName task name
     * @param shortDescription task short description
     * @param description task long description
     * @param status task status | null, completed
     * @param created_at task created date
     * @param updated_at task updated date
     * @param deleted_at task deleted date
     */
    public Task(int id, String taskName, String shortDescription, String description, String status, String created_at, String updated_at, String deleted_at) {
        this.id = id;
        this.taskName = taskName;
        this.shortDescription = shortDescription;
        this.description = description;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.deleted_at = deleted_at;
    }

    /**
     * Task id getter
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Task name getter
     *
     * @return taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Task short description getter
     *
     * @return shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * Task long description getter
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Task status getter
     *
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Task created date getter
     *
     * @return created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * Task updated date getter
     *
     * @return updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * Task deleted date getter
     *
     * @return deleted_at
     */
    public String getDeleted_at() {
        return deleted_at;
    }

    /**
     * Task id setter
     *
     * @param id task id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Task name setter
     *
     * @param taskName task name
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * Task short description setter
     *
     * @param shortDescription task short description
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * Task description setter
     *
     * @param description task description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Task status setter
     *
     * @param status task status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Task created at setter
     *
     * @param created_at task created at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * Task updated at setter
     *
     * @param updated_at task updated at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * Task deleted at setter
     *
     * @param deleted_at task deleted at
     */
    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }
}
