package com.assignment.mad.todo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.assignment.mad.todo.Database.TasksDatabaseHandler;
import com.assignment.mad.todo.EditTask;
import com.assignment.mad.todo.Models.Task;
import com.assignment.mad.todo.R;
import com.assignment.mad.todo.SingleTask;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Task Adapter
 * This Adapter is used to display Task Model data to the view component/UI
 * This is relevant to a component created in a web based framework to loop over a certain collection of data
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    // list of tasks from task model
    protected List<Task> tasks;
    // task completed constant
    protected String TASK_COMPLETED_CONST = "completed";
    // context
    protected Context context;
    // task database handler
    TasksDatabaseHandler tasksDatabaseHandler;

    /**
     * TaskAdapter Default Constructor
     *
     * @param tasks list of tasks
     * @param context context
     */
    public TaskAdapter(List<Task> tasks, Context context) {
        this.tasks = tasks;
        this.context = context;
    }

    // default on view holder create
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // get the parent context
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // find the task activity
        View taskActivity = inflater.inflate(R.layout.task_row, parent, false);

        // task database handler
        tasksDatabaseHandler = new TasksDatabaseHandler(context);

        // create a new view holder from task activity
        return new ViewHolder(taskActivity);
    }

    // on view holder bind
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // get the current task
        Task task = tasks.get(position);

        // if the current tasks status is completed the mark it as checked
        // checks if the status string is not empty and then checks if its equal to TASK_COMPLETED_CONST
        String taskStatus = task.getStatus();
        holder.taskDoneCheckbox.setChecked(! TextUtils.isEmpty(taskStatus) && taskStatus.equals(TASK_COMPLETED_CONST));

        // sets the text of task name
        holder.taskName.setText(task.getTaskName());

        // sets the text of task short description
        holder.taskDetailShort.setText(task.getShortDescription());

        // on task click, handle showing single task view
        holder.taskItem.setOnClickListener(new View.OnClickListener() {

            // default onclick from interface
            @Override
            public void onClick(View view) {
                // creates a new instance of single task intent
                Intent singleTaskIntent = new Intent(context, SingleTask.class);
                // puts extra strings to be passed in to the intent
                // task name, short description, description, status and date created
                singleTaskIntent.putExtra("taskName", task.getTaskName());
                singleTaskIntent.putExtra("shortDescription", task.getShortDescription());
                singleTaskIntent.putExtra("description", task.getDescription());
                singleTaskIntent.putExtra("status", task.getStatus());
                singleTaskIntent.putExtra("dateCreated", task.getCreated_at());
                // start activity
                context.startActivity(singleTaskIntent);
            }

        });

        // on check box click
        holder.taskDoneCheckbox.setOnClickListener(new View.OnClickListener() {

            // default onclick from interface
            @Override
            public void onClick(View view) {
                // updates the task status
                tasksDatabaseHandler.updateTaskStatus(task);
                // gets the tasks from db
                tasks = tasksDatabaseHandler.getTasks();
                // updates the adapter
                updateTaskList(tasks);

                // show success toast
                Toast.makeText(context, "Task Status Updated Successfully!", Toast.LENGTH_SHORT).show();
            }
        });

        // on edit button press
        holder.editButton.setOnClickListener(new View.OnClickListener() {

            // default onclick from interface
            @Override
            public void onClick(View view) {
                // start a new edit task activity
                Intent editIntent = new Intent(context, EditTask.class);
                // puts extra strings to be passed in to the intent
                // task name, short description AND description
                editIntent.putExtra("taskId", Integer.toString(task.getId()));
                editIntent.putExtra("taskName", task.getTaskName());
                editIntent.putExtra("shortDescription", task.getShortDescription());
                editIntent.putExtra("description", task.getDescription());
                editIntent.putExtra("status", task.getStatus());
                context.startActivity(editIntent);
            }
        });

        // on delete button press
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {

            // default onclick from interface
            // I really need to stop writing these useless comments xD
            @Override
            public void onClick(View view) {
                // updates the task status
                tasksDatabaseHandler.deleteTask(task);
                // gets the tasks from db
                tasks = tasksDatabaseHandler.getTasks();
                // updates the adapter
                updateTaskList(tasks);

                // show success toast
                Toast.makeText(context, "Task Deleted Successfully!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // returns the count/length/size of tasks list
    @Override
    public int getItemCount() {
        return tasks.size();
    }

    // prepares the view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        // create a new CheckBox variable for task done check box
        public CheckBox taskDoneCheckbox;
        // create a new TextView variable for task name
        public TextView taskName;
        // create a new checkbox variable for task details short/short description
        public TextView taskDetailShort;
        // layout. to make each task clickable
        public LinearLayout taskItem;
        // delete and edit image button
        public ImageButton deleteButton, editButton;

        /**
         * Default ViewHolder class constructor
         * Finds the elements by id and assign to above variables
         *
         * @param view item view
         */
        public ViewHolder (@NonNull View view) {
            super(view);
            taskDoneCheckbox = view.findViewById(R.id.taskDoneCheckbox);
            taskName = view.findViewById(R.id.taskName);
            taskDetailShort = view.findViewById(R.id.taskDetailShort);
            taskItem = view.findViewById(R.id.taskItem);
            deleteButton = view.findViewById(R.id.deleteButton);
            editButton = view.findViewById(R.id.editButton);
        }
    }

    /**
     * Update Tasks
     *
     * @param tasks tasks
     */
    public void updateTaskList(List<Task> tasks) {
        // set new list
        this.tasks = tasks;
        // notify changes
        this.notifyDataSetChanged();
    }
}
