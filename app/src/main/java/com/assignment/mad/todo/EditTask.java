package com.assignment.mad.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Edits/Updates a single Task
 */
import com.assignment.mad.todo.Database.TasksDatabaseHandler;
import com.assignment.mad.todo.Models.Task;

/**
 * This class/activity handles the editing of the tasks
 * When a user taps the edit button on a specific task, this class will initiate with the relative activity
 * The users can then edit their existing task accordingly
 * This class gets the current task, displays its current values in the text inputs and then allows the users
 * to modify the values.
 * Database will then trigger sqlite update query on the task provided with the relevant information entered/modified
 */
public class EditTask extends AppCompatActivity {

    // tasks database handler
    TasksDatabaseHandler databaseHandler;

    // task name, short description, description
    protected EditText taskNameInput, taskShortDescriptionInput, taskDescriptionInput;

    // update button
    protected Button updateTaskButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        // set the back button on action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // instantiate database handler
        databaseHandler = new TasksDatabaseHandler(this);

        // update task button
        updateTaskButton = findViewById(R.id.updateTaskButton);

        // get task name, short description, description, status and date created
        String taskId = getIntent().getStringExtra("taskId");
        String taskName = getIntent().getStringExtra("taskName");
        String shortDescription = getIntent().getStringExtra("shortDescription");
        String description = getIntent().getStringExtra("description");
        String status = getIntent().getStringExtra("status");

        // sets the action bar title to task name
        // if the task name is set, then show tasks name else show a static 'task' text
        getSupportActionBar().setTitle(! TextUtils.isEmpty(taskName) ? taskName : "Task");

        // assign elements to variables by finding them on activity view
        this.taskNameInput = findViewById(R.id.taskNameInput);
        this.taskShortDescriptionInput = findViewById(R.id.taskShortDescriptionInput);
        this.taskDescriptionInput = findViewById(R.id.taskDescriptionInput);

        // assign values to elements
        this.taskNameInput.setText(taskName);
        this.taskShortDescriptionInput.setText(shortDescription);
        this.taskDescriptionInput.setText(description);

        // on update click
        updateTaskButton.setOnClickListener(new View.OnClickListener() {

            // on click
            @Override
            public void onClick(View view) {
                // create a new task from the current data
                Task task = new Task(Integer.parseInt(taskId), taskNameInput.getText().toString(), taskShortDescriptionInput.getText().toString(), taskDescriptionInput.getText().toString(), status);
                // update the task
                databaseHandler.updateTask(task);

                // update task
                Toast.makeText(EditTask.this, "Task Updated Successfully!", Toast.LENGTH_SHORT).show();

                // fin. finally
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // for two different versions of API
        // this handles the onclick for back button press on the action bar
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}