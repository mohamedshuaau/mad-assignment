package com.assignment.mad.todo.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * This class is a helper class which creates all the tables and handles db updates etc
 * As the application has only 1 table, this class is simplified
 * Other classes which wants to create data can call this class
 */
public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    // database version and the database name definition
    public static final int DATABASE_VERSION = 1;
    // database name
    public static final String DATABASE_NAME = "Actify";
    // tasks table name
    public static final String TASKS_TABLE_NAME = "tasks";

    // sql create table query
    private static final String SQL_CREATE_QUERY =
            "CREATE TABLE " + TASKS_TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "name VARCHAR(100) NOT NULL,\n" +
                    "short_description VARCHAR(100) NOT NULL,\n" +
                    "description VARCHAR(100) NOT NULL,\n" +
                    "status VARCHAR(100),\n" +
                    "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,\n" +
                    "updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,\n" +
                    "deleted_at TIMESTAMP)";

    // sql table drop query
    private static final String SQL_DROP_QUERY =
            "DROP TABLE IF EXISTS " + TASKS_TABLE_NAME;

    // default constructor
    public SQLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // on create, call the create entries which will initiate the table and required queries
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_QUERY);
    }

    // if the version changes (it then assumes the database content changed) then drop and recreate the db
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_QUERY);
        onCreate(db);
    }

    // on downgrade, do the above (upgrade)
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
