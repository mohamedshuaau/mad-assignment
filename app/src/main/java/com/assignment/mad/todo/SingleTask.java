package com.assignment.mad.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.TextView;

import com.assignment.mad.todo.Database.TasksDatabaseHandler;

/**
 * This class/activity would display the information of the single task
 * Much like the edit, when a specific task is clicked, this activity gets triggered and the tasks
 * information is then displayed on text views
 * Information on this screen includes the long description of the task as well
 */
public class SingleTask extends AppCompatActivity {

    // task name, short description, description, status and created at variables
    protected TextView taskName, shortDescription, description, status, createdAt;

    // tasks database handler
    TasksDatabaseHandler databaseHandler;

    // task completed/pending constant
    protected String TASK_COMPLETED_CONST = "completed", TASK_PENDING_CONST = "pending";

    // default on create method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_task);

        // set the back button on action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // instantiate database handler
        databaseHandler = new TasksDatabaseHandler(this);

        // get task name, short description, description, status and date created
        String taskName = getIntent().getStringExtra("taskName");
        String shortDescription = getIntent().getStringExtra("shortDescription");
        String description = getIntent().getStringExtra("description");
        String status = getIntent().getStringExtra("status");
        String dateCreated = getIntent().getStringExtra("dateCreated");

        // sets the action bar title to task name
        // if the task name is set, then show tasks name else show a static 'task' text
        getSupportActionBar().setTitle(! TextUtils.isEmpty(taskName) ? taskName : "Task");

        // assign elements to variables by finding them on activity view
        this.taskName = findViewById(R.id.taskName);
        this.shortDescription = findViewById(R.id.taskShortDescription);
        this.description = findViewById(R.id.taskDescription);
        this.status = findViewById(R.id.taskStatus);
        this.createdAt = findViewById(R.id.taskCreatedAt);

        // assign values to elements
        this.taskName.setText(taskName);
        this.shortDescription.setText(shortDescription);
        this.description.setText(description);

        // if the task is set and is completed, then set the text
        if (! TextUtils.isEmpty(status) && status.equals(TASK_COMPLETED_CONST)) {
            this.status.setText(TASK_COMPLETED_CONST);
        } else {
            this.status.setText(TASK_PENDING_CONST);
        }

        // set date
        this.createdAt.setText(dateCreated);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // for two different versions of API
        // this handles the onclick for back button press on the action bar
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}