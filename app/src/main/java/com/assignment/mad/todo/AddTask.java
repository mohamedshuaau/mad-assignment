package com.assignment.mad.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.assignment.mad.todo.Database.TasksDatabaseHandler;
import com.assignment.mad.todo.Models.Task;

/**
 * This class/activity handles the creation of new tasks
 * When the user clicks the floating button on main activity, this activity will be created
 * Database will then trigger sqlite insert query on the task provided with the relevant information entered
 */
public class AddTask extends AppCompatActivity {

    // inputs variables
    EditText taskNameInput, taskShortDescriptionInput, taskDescriptionInput;

    // add task button
    Button addTaskButton;

    // database handler for tasks model
    TasksDatabaseHandler db;

    // default on create method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        // set the back button on action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // sets the action bar title
        getSupportActionBar().setTitle("Create Task");

        // instantiate and assign tasks database handler
        db = new TasksDatabaseHandler(this);

        // assign inputs from view
        taskNameInput = findViewById(R.id.taskNameInput);
        taskShortDescriptionInput = findViewById(R.id.taskShortDescriptionInput);
        taskDescriptionInput = findViewById(R.id.taskDescriptionInput);

        // assign the add task button
        addTaskButton = findViewById(R.id.addTaskButton);

        // on create button click
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // create a new task
                Task task = new Task(taskNameInput.getText().toString(), taskShortDescriptionInput.getText().toString(), taskDescriptionInput.getText().toString());

                // create the task
                long newId = db.createNewTask(task);

                // if the id is less than or equal to 0, then its safe to assume it failed
                // no error handlings just show a toast
                if (newId <= 0) {
                    Toast.makeText(AddTask.this, "Something went wrong while trying to create a new Task", Toast.LENGTH_SHORT).show();
                    return;
                }

                // toast a success message
                Toast.makeText(AddTask.this, "Created a new Task", Toast.LENGTH_SHORT).show();

                // finish
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // for two different versions of API
        // this handles the onclick for back button press on the action bar
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}