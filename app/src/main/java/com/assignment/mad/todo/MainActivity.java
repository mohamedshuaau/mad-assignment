package com.assignment.mad.todo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.assignment.mad.todo.Adapters.TaskAdapter;
import com.assignment.mad.todo.Database.TasksDatabaseHandler;
import com.assignment.mad.todo.Models.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * Main Activity Class
 * This will be called/instantiated once the splash screen is unmounted
 * This class contains the list of tasks and a floating button which the users can tap to go to the
 * add task activity/screen
 */
public class MainActivity extends AppCompatActivity {

    // tasks list
    protected List<Task> tasks;

    // task adapter
    protected TaskAdapter taskAdapter;

    // recycler view
    protected RecyclerView recyclerView;

    // database handler
    protected TasksDatabaseHandler databaseHandler;

    // default on create method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // sets the action bar title
        getSupportActionBar().setTitle("Tasks");

        // instantiate tasks sqlite database handler
        databaseHandler = new TasksDatabaseHandler(this);

        // instantiate the TaskAdapter
        taskAdapter = new TaskAdapter(tasks, this);

        // get the recycler view from task_row activity
        recyclerView = findViewById(R.id.taskRecyclerView);

        recyclerView.setAdapter(taskAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // find the floating button by id
        FloatingActionButton addTaskButton = findViewById(R.id.addTaskButton);

        // add an onclick even listener on the floating button
        addTaskButton.setOnClickListener(new View.OnClickListener() {
            // on click to floating button
            @Override
            public void onClick(View view) {
                // creates a new addTask intent
                Intent addTask = new Intent(MainActivity.this, AddTask.class);

                // start the addTask intent/activity
                startActivity(addTask);
            }
        });
    }

    // on resume
    // this assumes that where ever we are, as soon as we return to this intent, it will trigger a reload on data
    // so its safe to assume that the user was either at update, insert or delete and returned back here so update the data
    // making less and less sense... NEED COFFEE
    // removed on create getTasks() method call as on resume is called either way
    @Override
    protected void onResume() {
        super.onResume();

        // get task data
        tasks = databaseHandler.getTasks();

        // updates the list
        taskAdapter.updateTaskList(tasks);

    }
}