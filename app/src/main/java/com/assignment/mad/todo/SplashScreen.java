package com.assignment.mad.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Splash Screen Activity Class
 * This is responsible for showing a simple delayed splash screen
 * The splash screen lasts 2 seconds but this can be modified with the constant variable DELAY_MILLISECOND
 */
public class SplashScreen extends AppCompatActivity {

    // delay in milliseconds for the main intent to run
    protected int DELAY_MILLISECOND = 2000;

    // default on create method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // hides the Action bar on create
        // hiding action bar from here instead of using NoActionBar theme inside on android manifest for this screen,
        // results in removing the status bar color as well hence hiding the action bar would still maintain the status
        // bar colors
        getSupportActionBar().hide();

        // posts a delay of DELAY_MILLISECOND milliseconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // creates a new intent
                Intent mainIntent = new Intent(SplashScreen.this, MainActivity.class);
                // start the new intent
                startActivity(mainIntent);
                // finally remove the intent
                finish();
            }
        }, DELAY_MILLISECOND);
    }
}